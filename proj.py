# coding: utf-8
from __future__ import print_function

import cv2
import numpy as np
import random

# Preprocess picture before training
def preprocessPicture(image):
    random.seed()

    rand=[]
    for i in range(6):
        rand.append(random.random())

    rotate = False
    #rotate
    if rand[0] > 0.7:
        angle = random.uniform(-5,5)
        image_center = tuple(np.array(image.shape[1::-1]) / 2)
        rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
        image = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
        rotate = True
        
    #blur
    if rand[1] > 0.8:
        image = cv2.GaussianBlur(image,(5,5),0)

    if rand[2] > 0.75:    
        hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV) #convert it to hsv

        h, s, v = cv2.split(hsv)
        for k in v:
            for x in k:
                if (x + 255) > 255:
                    x=255    #brightness
                else: 
                    x+=255

        final_hsv = cv2.merge((h, s, v))
        image = cv2.cvtColor(final_hsv, cv2.COLOR_HSV2BGR)

    if rand[3] > 0.75:
        contrast = random.uniform(-15,25)
        f = 131*(contrast + 127)/(127*(131-contrast))
        alpha_c = f
        gamma_c = 127*(1-f)
        image = cv2.addWeighted(image, alpha_c, image, 0, gamma_c)
      
    #noise
    if rand[4] > 0.8:
        row,col,ch= image.shape
        mean = 0
        var = 0.1
        sigma = var**0.5
        gauss = np.random.normal(mean,sigma,(row,col,ch))
        gauss = gauss.reshape(row,col,ch)
        image = image + gauss*25

    height, width = image.shape[:2]
    
    if rotate:
        shift = 5
        image = image[shift:height-shift, shift:width-shift]

    if rand[5] > 0.65:
        direction = random.random()
        #change size of image and crop image in some direction
        change = 10
        pomer = height/width
        #down
        if direction <0.25:
            w = (height-change)/pomer
            new_w = int((width-w)/2)
            image = image[change:height, new_w:width-new_w]
        #up
        elif direction <0.5:
            w = (height-change)/pomer
            new_w = int((width-w)/2)
            image = image[0:height-change, new_w:width-new_w]
        #left
        elif direction <0.75:
            h = pomer*(width-change)
            new_h = int((height-h)/2)
            image = image[new_h:height-new_h, 0:width-change]
        #right
        else:
            h = pomer*(width-change)
            new_h = int((height-h)/2)
            image = image[new_h:height-new_h, change:width]

    #change size if was changed
    image = cv2.resize(image,(width, height), interpolation = cv2.INTER_CUBIC)
    return image
