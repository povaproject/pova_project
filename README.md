# README #

Pro spuštění s detekcí použijte přepínač -d

usage: network.py [-h] [-d] [-t] [-l FILE] file [file ...]
  -h, --help  show this help message and exit
  -d          Detection mode
  -t          Train network
  -l LOAD     Load network