import os
import numpy as np
from matplotlib import pyplot as plt
from proj import preprocessPicture
import cv2
import argparse

IMAGE_SIZE = 48
EPOCHS = 30
PROB_THRESHOLD = 0.7

classes = ['00000','10000','10001','10002','10003','10004','10005','10006','10007','10010','10011','10012','10013','10014','10015','10016','10017','10018','10021','10030','10031','20019','20060','20061','30005','30010','30015','30020','30025','30030','30040','30050','30060','30070','30080','30100','30120','40001','40002','40003','40004','40005','40006','40007','40008','40020','40033','40037','50020','50022','50023','50025','50028','50029','50030','50031','50040','50041','55001','55002','60044','60053','60054','60056']

class_names = ['Other','Uneven road','Hump','Slippery road','Bend to left','Bend to right','Double bend left','Double bend right','School crossing','Road work','Traffic lights ahead','Railways crossing','Danger','Road narrow on both sides','Road narrow on left side','Road narrow on right side','Crossroad ahead','Crossroad ahead','Stop and yield','Snow and ice','Wild animals','Give way','Priority road end','Priority road','Maximum speed 5','Maximum speed 10','Maximum speed 15','Maximum speed 20','Maximum speed 25','Maximum speed 30','Maximum speed 40','Maximum speed 50','Maximum speed 60','Maximum speed 70','Maximum speed 80','Maximum speed 100','Maximum speed 120','Go right','Go left','Go straight','Go straight or right','Go straight or left','Keep right','Keep left','Go left or right','Fast traffic highway','Cyclists and pedestrians','Roundabout','Give priority to vehicles from opposite direction','No entry','No cycling','No truck','No entry','No left turn','No right turn','No overtaking','No waiting','No stopping','End of all restrictions','End of no overtaking','Priority over oncoming vehicles','One-way traffic','Dead end street','Pedestrian crossing ahead']


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', dest='detector', help="Detection mode", action='store_true')
    parser.add_argument('-t', dest='train', help="Train network", action='store_true')
    parser.add_argument('-l', dest='load', help="Load network")
    parser.add_argument('file', nargs="+",  help="Input files")
    return parser.parse_args()
    
# plot images with labels
def show_dataset(images, labels):
    label_set = set(labels)
    fig = plt.figure(figsize=(20, 20))
    fig.subplots_adjust(wspace=3.0, hspace=None)
    i = 1
    for label in label_set:
        image = images[labels.index(label)].copy()
        image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        count = labels.count(label)
        title = classes[int(label)]
        
        fig = plt.subplot(10, 8, i)
        plt.axis('off')
        plt.title("{0} ({1})".format(title, count))
        plt.imshow(cv2.cvtColor(image, cv2.COLOR_RGB2BGR))
        i += 1
    plt.show()

# build network
def build_network(classes_count):
    from keras.layers import Dense, Dropout, Flatten, BatchNormalization
    from keras.layers import Conv2D, MaxPooling2D
    from keras.models import Model, Sequential

    model = Sequential()
        
    model.add(Conv2D(32, (3, 3), activation='relu', padding='same', data_format="channels_first", input_shape=(3, IMAGE_SIZE, IMAGE_SIZE)))
    model.add(Conv2D(32, (3, 3), activation='relu', data_format="channels_first"))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Conv2D(64, (3, 3), activation='relu', padding='same', data_format="channels_first"))
    model.add(Conv2D(64, (3, 3), activation='relu', data_format="channels_first"))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Conv2D(128, (3, 3), activation='relu', padding='same', data_format="channels_first"))
    model.add(Conv2D(128, (3, 3), activation='relu', data_format="channels_first"))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))
    model.add(Flatten())
    model.add(Dense(256, activation='relu'))
    model.add(Dropout(0.5))
    model.add(Dense(classes_count, activation='softmax'))
    
    return model
    
# resize image
def parse_image(image):
    image = cv2.resize(image, (IMAGE_SIZE, IMAGE_SIZE)) 
    return image

# load images and label from directory
#     @source - directory containing folders with data
#     @format - image file extension
def load_data(source, format):
    # get all folders
    dirs = os.listdir(source) 
    folders = [dir for dir in dirs if os.path.isdir(os.path.join(source, dir))]
    
    # get all images from directories
    labels = []
    images = []
    for folder in folders:
        folder_path = os.path.join(source, folder)
        files = [os.path.join(folder_path, file) 
                      for file in os.listdir(folder_path) 
                      if file.endswith("."+format)]
        for file in files:
            image = cv2.imread(file)
            image = parse_image(image)
            images.append(image)
            labels.append(classes.index(folder))

    return images, labels

# append data to list
def addData(trnData, trnLabels, path, format):
    trnDataTemp, trnLabelsTemp = load_data(path, format)
    trnData.extend(trnDataTemp)
    trnLabels.extend(trnLabelsTemp)
    
    return trnData, trnLabels

# create and train network
def create():    
    # load train data
    trnData = []
    trnLabels = []    
    print("Loading train data Other...")
    trnData, trnLabels = addData(trnData, trnLabels, "datasets/Other", "jpg")
    print("Loading train data Belgium...")
    trnData, trnLabels = addData(trnData, trnLabels, "datasets/BelgiumTSC/Training", "ppm")
    print("Loading train data GTSRB...")
    trnData, trnLabels = addData(trnData, trnLabels, "datasets/GTSRB_Final_Training_Images/Images", "ppm")
    print("Loading train data German...")
    trnData, trnLabels = addData(trnData, trnLabels, "datasets/German/Training", "png")    
    print("Train data loaded")
    
    #load test data
    tstData = []
    tstLabels = []
    print("Loading test data Belgium...")
    tstData, tstLabels = addData(tstData, tstLabels, "datasets/BelgiumTSC/Testing", "ppm")
    print("Loading test data German...")
    tstData, tstLabels = addData(tstData, tstLabels, "datasets/German/Training", "png")
    print("Test data loaded")

    # show data
    #show_dataset(trnData, trnLabels)
    #show_dataset(tstData, tstLabels)
    
    # build network
    model = build_network(len(set(trnLabels)))
    print('Model summary:')
    model.summary()
    
    from keras import optimizers
    from keras import losses
    from keras import metrics
    
    # compile network
    print("Compiling NN")
    model.compile(
        loss=losses.sparse_categorical_crossentropy,
        optimizer=optimizers.Adam(lr=0.001),
        metrics=[metrics.sparse_categorical_accuracy])
        
    max_acc = 0.8

    # train network
    for epoch in range(EPOCHS):
        
        # augment data
        print("Processing data augmentation...")
        trnData_augmented = trnData[:].copy()
        trnData_augmented[:] = [preprocessPicture(image) for image in trnData_augmented]
        print("Training epoch {0}/{1}".format(epoch+1, EPOCHS))
        
        # train model
        history = model.fit(
            x=np.array([np.rollaxis(image, 2, 0) for image in trnData_augmented]), y=np.array(trnLabels),
            batch_size=64, epochs=1, verbose=1,
            validation_data=[np.array([np.rollaxis(image, 2, 0) for image in tstData]), tstLabels], shuffle=True)

        # save if better accuracy
        acc = history.history['val_sparse_categorical_accuracy'][0]
        print("accuracy: {0}".format(acc))
        if acc > max_acc:            
            print("saving model!")
            name = "output/network_{0}.h5".format(acc)
            model.save(name)
            max_acc = acc
  
    return model

# classify data
def classify(model, image):
    # process image
    image = parse_image(image)   
    image = np.rollaxis(image, 2, 0)
    image = np.expand_dims(image, axis=0)
    
    # predict
    classProb = model.predict(image)[0]   
    predictedClass = np.argmax(classProb)
    propability = classProb[predictedClass]

    # probability is not high enough
    if propability < PROB_THRESHOLD:
        return None
    
    # class 'other'
    if classes[predictedClass] == '00000':
        return None
    
    return classes[predictedClass]
    

def main():            
    #create and train model
    args = parse_arguments()
        
    if args.train:
        model = create() 
        return 0
    elif args.load:
        from keras import models
        model = models.load_model(args.load)
    else:
        from keras import models
        model = models.load_model('network.h5')
    #load models
    detectors = list()
    if args.detector:
        #one detector for each main class of sign
        detectors.append(cv2.CascadeClassifier('cascadeA.xml'))
        detectors.append(cv2.CascadeClassifier('cascadeB.xml'))
        detectors.append(cv2.CascadeClassifier('cascadeC.xml'))
        detectors.append(cv2.CascadeClassifier('cascadeD.xml'))
        detectors.append(cv2.CascadeClassifier('cascadeE.xml'))
        detectors.append(cv2.CascadeClassifier('cascadeF.xml'))            
    
    
    # Process the files and draw results
    for f in args.file:
        print("File:", f)
        # classify image
        image = cv2.imread(f)
        
        result = None
        font = cv2.FONT_HERSHEY_SIMPLEX
        if args.detector:
            for detector in detectors:
                bbs = detector.detectMultiScale(image, scaleFactor=1.1, minNeighbors=15)
                for (x,y,w,h) in bbs:
                    crop_img = image[y:y+h, x:x+w]
                    result = classify(model, crop_img)
                    if result != None:
                        cv2.rectangle(image,(x,y),(x+w,y+h),(0,255,0),4)
                        cv2.putText(image,class_names[classes.index(result)],(x,y), font, 0.5,(255,255,0),2,cv2.LINE_AA)
                            
            cv2.imshow("Image", image)
        else:
            result = classify(model, image)

        if result != None:
            print("result: " + class_names[classes.index(result)])
        else:
            print("result: " + "no class")
        
        key = cv2.waitKey() & 0xFF
        if key == ord('q'):
            continue
    
if __name__ == "__main__":
    main()
